package controllers

import (
	"net/http"
	"time"

	"backend/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type StructTransactionInput struct {
	ID           int `json:"id" gorm:"primary_key"`
	Payment      int `json:"payment"`
	StructUserID int `json:"user_ID"`
}

// GetAllStructTransaction godoc
// @Summary Get all StructTransaction.
// @Description Get a list of StructTransaction.
// @Tags StructTransaction
// @Produce json
// @Success 200 {object} []models.StructTransaction
// @Router /data-transaction [get]
func GetAllTransaction(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var transactions []models.StructTransaction
	db.Find(&transactions)

	c.JSON(http.StatusOK, gin.H{"data": transactions})
}

// CreateStructTransaction godoc
// @Summary Create New StructTransaction.
// @Description Creating a new StructTransaction.
// @Tags StructTransaction
// @Param Body body StructTransactionInput true "the body to create a new StructTransaction"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.StructTransaction
// @Router /data-transaction [post]
func CreateTransaction(c *gin.Context) {
	// Validate input
	var input StructTransactionInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Transaction
	transaction := models.StructTransaction{
		// ID:          input.ID,
		// NameProduct: input.NameProduct,
		// Image:       input.Image,
		// Price:       input.Price,
		// Rating:      input.Rating,
		// Description: input.Description,
		ID:           input.ID,
		Payment:      input.Payment,
		StructUserID: input.StructUserID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&transaction)

	c.JSON(http.StatusOK, gin.H{"data": transaction})
}

// GetStructTransactionById godoc
// @Summary Get StructTransaction.
// @Description Get an StructTransaction by id.
// @Tags StructTransaction
// @Produce json
// @Param id path string true "StructTransaction id"
// @Success 200 {object} models.StructTransaction
// @Router /data-transaction/{id} [get]
func GetTransactionById(c *gin.Context) { // Get model if exist
	var transaction models.StructTransaction

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&transaction).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": transaction})
}

// UpdateStructTransaction godoc
// @Summary Update StructTransaction.
// @Description Update StructTransaction by id.
// @Tags StructTransaction
// @Produce json
// @Param id path string true "StructTransaction id"
// @Param Body body StructTransactionInput true "the body to update transaction"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.StructTransaction
// @Router /data-transaction/{id} [patch]
func UpdateTransaction(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var transaction models.StructTransaction
	if err := db.Where("id = ?", c.Param("id")).First(&transaction).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input StructTransactionInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.StructTransaction
	updatedInput.ID = input.ID
	updatedInput.Payment = input.Payment
	updatedInput.StructUserID = input.StructUserID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&transaction).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": transaction})
}

// DeleteStructTransaction godoc
// @Summary Delete one StructTransaction.
// @Description Delete a StructTransaction by id.
// @Tags StructTransaction
// @Produce json
// @Param id path string true "StructTransaction id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /data-transaction/{id} [delete]
func DeleteTransaction(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var transaction models.StructTransaction
	if err := db.Where("id = ?", c.Param("id")).First(&transaction).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&transaction)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
