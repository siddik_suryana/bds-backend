package controllers

import (
	"net/http"
	"time"

	"backend/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type StructProductInput struct {
	ID               int     `json:"id" gorm:"primary_key"`
	NameProduct      string  `json:"name_product"`
	Image            string  `json:"image"`
	Price            int     `json:"price"`
	Rating           float64 `json:"rating"`
	Description      string  `json:"description"`
	StructUserID     int     `json:"userID"`
	StructCategoryID int     `json:"categoryID"`
}

// GetAllStructProduct godoc
// @Summary Get all StructProduct.
// @Description Get a list of StructProduct.
// @Tags StructProduct
// @Produce json
// @Success 200 {object} []models.StructProduct
// @Router /data-product [get]
func GetAllProduct(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var products []models.StructProduct
	db.Find(&products)

	c.JSON(http.StatusOK, gin.H{"data": products})
}

// CreateStructProduct godoc
// @Summary Create New StructProduct.
// @Description Creating a new StructProduct.
// @Tags StructProduct
// @Param Body body StructProductInput true "the body to create a new StructProduct"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.StructProduct
// @Router /data-product [post]
func CreateProduct(c *gin.Context) {
	// Validate input
	var input StructProductInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Product
	product := models.StructProduct{
		ID:          input.ID,
		NameProduct: input.NameProduct,
		Image:       input.Image,
		Price:       input.Price,
		Rating:      input.Rating,
		Description: input.Description,
		StructCategoryID: input.StructCategoryID,
		StructUserID:     input.StructUserID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&product)

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// GetStructProductById godoc
// @Summary Get StructProduct.
// @Description Get an StructProduct by id.
// @Tags StructProduct
// @Produce json
// @Param id path string true "StructProduct id"
// @Success 200 {object} models.StructProduct
// @Router /data-product/{id} [get]
func GetProductById(c *gin.Context) { // Get model if exist
	var product models.StructProduct

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": product})
}


// UpdateStructProduct godoc
// @Summary Update StructProduct.
// @Description Update StructProduct by id.
// @Tags StructProduct
// @Produce json
// @Param id path string true "StructProduct id"
// @Param Body body StructProductInput true "the body to update Product"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.StructProduct
// @Router /data-product/{id} [patch]
func UpdateProduct(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var product models.StructProduct
	if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input StructProductInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.StructProduct
	updatedInput.ID = input.ID
	updatedInput.NameProduct = input.NameProduct
	updatedInput.Image = input.Image
	updatedInput.Price = input.Price
	updatedInput.Rating = input.Rating
	updatedInput.Description = input.Description
	updatedInput.StructCategoryID = input.StructCategoryID
	updatedInput.StructUserID = input.StructUserID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&product).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// DeleteStructProduct godoc
// @Summary Delete one StructProduct.
// @Description Delete a StructProduct by id.
// @Tags StructProduct
// @Produce json
// @Param id path string true "StructProduct id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /data-product/{id} [delete]
func DeleteProduct(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var product models.StructProduct
	if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&product)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
