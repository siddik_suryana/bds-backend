package routes

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"backend/controllers"
	"backend/middlewares"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowHeaders = []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"}

	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true

	// OPTIONS method for ReactJS
	corsConfig.AddAllowMethods("OPTIONS")

	r.Use(cors.New(corsConfig))

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

    r.POST("/register", controllers.Register)
    r.POST("/login", controllers.Login)

	r.GET("/data-transaction", controllers.GetAllTransaction)
	r.GET("/data-transaction/:id", controllers.GetTransactionById)
	transactionMiddlewareRoute := r.Group("/data-transaction")
	transactionMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	transactionMiddlewareRoute.POST("/", controllers.CreateTransaction)
	transactionMiddlewareRoute.PATCH("/:id", controllers.UpdateTransaction)
	transactionMiddlewareRoute.DELETE("/:id", controllers.DeleteTransaction)

	r.GET("/data-user", controllers.GetAllUser)
	r.GET("/data-user/:id", controllers.GetUserById)
	userMiddlewareRoute := r.Group("/data-user")
	userMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	userMiddlewareRoute.POST("/", controllers.CreateUser)
	userMiddlewareRoute.PATCH("/:id", controllers.UpdateUser)
	userMiddlewareRoute.DELETE("/:id", controllers.DeleteUser)

	r.GET("/data-category", controllers.GetAllCategory)
	r.GET("/data-category/:id", controllers.GetCategoryById)
	categoryMiddlewareRoute := r.Group("/data-category")
	categoryMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	categoryMiddlewareRoute.POST("/", controllers.CreateCategory)
	categoryMiddlewareRoute.PATCH("/:id", controllers.UpdateCategory)
	categoryMiddlewareRoute.DELETE("/:id", controllers.DeleteCategory)

	r.GET("/data-product", controllers.GetAllProduct)
	r.GET("/data-product/:id", controllers.GetProductById)
	r.GET("/data-product/:id/data-user", controllers.GetProductByUserId)
	productMiddlewareRoute := r.Group("/data-product")
	productMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	productMiddlewareRoute.POST("/", controllers.CreateProduct)
	productMiddlewareRoute.PATCH("/:id", controllers.UpdateProduct)
	productMiddlewareRoute.DELETE("/:id", controllers.DeleteProduct)

	r.GET("/data-feed", controllers.GetAllFeed)
	r.GET("/data-feed/:id", controllers.GetFeedById)
	// r.GET("/data-feed/:id/data-transaction", controllers.GetTransactionByFeedId)
	feedMiddlewareRoute := r.Group("/data-feed")
	feedMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	feedMiddlewareRoute.POST("/", controllers.CreateFeed)
	feedMiddlewareRoute.PATCH("/:id", controllers.UpdateFeed)
	feedMiddlewareRoute.DELETE("/:id", controllers.DeleteFeed)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
