package controllers

import (
	"net/http"
	"time"

	"backend/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type StructFeedInput struct {
	ID              int           `json:"id" gorm:"primary_key"`
	Comment         string        `json:"comment"`
	StructUserID    int           `json:"userID"`
	StructProductID int           `json:"productID"`
}

// GetAllStructFeed godoc
// @Summary Get all StructFeed.
// @Description Get a list of StructFeed.
// @Tags StructFeed
// @Produce json
// @Success 200 {object} []models.StructFeed
// @Router /data-feed [get]
func GetAllFeed(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var feeds []models.StructFeed
	db.Find(&feeds)

	c.JSON(http.StatusOK, gin.H{"data": feeds})
}

// CreateStructFeed godoc
// @Summary Create New StructFeed.
// @Description Creating a new StructFeed.
// @Tags StructFeed
// @Param Body body StructFeedInput true "the body to create a new StructFeed"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.StructFeed
// @Router /data-feed [post]
func CreateFeed(c *gin.Context) {
	// Validate input
	var input StructFeedInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Feed
	feed := models.StructFeed{
		ID:              input.ID,
		Comment:         input.Comment,
		StructUserID:    input.StructUserID,
		StructProductID: input.StructProductID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&feed)

	c.JSON(http.StatusOK, gin.H{"data": feed})
}

// GetStructFeedById godoc
// @Summary Get StructFeed.
// @Description Get an StructFeed by id.
// @Tags StructFeed
// @Produce json
// @Param id path string true "StructFeed id"
// @Success 200 {object} models.StructFeed
// @Router /data-feed/{id} [get]
func GetFeedById(c *gin.Context) { // Get model if exist
	var feed models.StructFeed

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&feed).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": feed})
}

// GetTransactionByFeedID godoc
// @Summary Get Transaction.
// @Description Get all Transaction by StructFeedID.
// @Tags StructFeed
// @Produce json
// @Param id path string true "StructFeed id"
// @Success 200 {object} []models.StructFeed
// @Router /data-feed/{id}/data-feed [get]
func GetFeedByUserId(c *gin.Context) { // Get model if exist
	var feed []models.StructFeed

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("userID = ?", c.Param("id")).Find(&feed).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return

	}

	c.JSON(http.StatusOK, gin.H{"data": feed})
}

// UpdateStructFeed godoc
// @Summary Update StructFeed.
// @Description Update StructFeed by id.
// @Tags StructFeed
// @Produce json
// @Param id path string true "StructFeed id"
// @Param Body body StructFeedInput true "the body to update feed"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.StructFeed
// @Router /data-feed/{id} [patch]
func UpdateFeed(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var feed models.StructFeed
	if err := db.Where("id = ?", c.Param("id")).First(&feed).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input StructFeedInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.StructFeed
	updatedInput.ID = input.ID
	updatedInput.Comment = input.Comment
	updatedInput.StructUserID = input.StructUserID
	updatedInput.StructProductID = input.StructProductID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&feed).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": feed})
}

// DeleteStructFeed godoc
// @Summary Delete one StructFeed.
// @Description Delete a StructFeed by id.
// @Tags StructFeed
// @Produce json
// @Param id path string true "StructFeed id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /data-feed/{id} [delete]
func DeleteFeed(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var feed models.StructFeed
	if err := db.Where("id = ?", c.Param("id")).First(&feed).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&feed)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
