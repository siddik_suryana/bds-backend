package models

import (
	"time"
)

type (
	StructProduct struct {
		ID            int             `json:"id" gorm:"primary_key"`
		NameProduct   string          `json:"name_product"`
		Image         string          `json:"image"`
		Price         int             `json:"price"`
		Rating        float64         `json:"rating"`
		Description   string          `json:"description"`
		StructCategoryID int            `json:"categoryID"`
		StructUserID     int            `json:"userID"`
		CreatedAt        time.Time      `json:"created_at"`
		UpdatedAt        time.Time      `json:"updated_at"`
		Feed             []StructFeed   `json:"-"`
		StructCategory   StructCategory `json:"-"`
		StructUser       StructUser     `json:"-"`
	}
)
