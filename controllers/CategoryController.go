package controllers

import (
	"net/http"
	"time"

	"backend/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type StructCategoryInput struct {
	ID               int            `json:"id" gorm:"primary_key"`
		NameCategory      string         `json:"name_category"`
}

// GetAllStructCategory godoc
// @Summary Get all StructCategory.
// @Description Get a list of StructCategory.
// @Tags StructCategory
// @Produce json
// @Success 200 {object} []models.StructCategory
// @Router /data-category [get]
func GetAllCategory(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var categorys []models.StructCategory
	db.Find(&categorys)

	c.JSON(http.StatusOK, gin.H{"data": categorys})
}

// CreateStructCategory godoc
// @Summary Create New StructCategory.
// @Description Creating a new StructCategory.
// @Tags StructCategory
// @Param Body body StructCategoryInput true "the body to create a new StructCategory"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.StructCategory
// @Router /data-category [post]
func CreateCategory(c *gin.Context) {
	// Validate input
	var input StructCategoryInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Category
	category := models.StructCategory{
		ID:               input.ID,
		NameCategory:      input.NameCategory,
		
		
	}	
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&category)

	c.JSON(http.StatusOK, gin.H{"data": category})
}

// GetStructCategoryById godoc
// @Summary Get StructCategory.
// @Description Get an StructCategory by id.
// @Tags StructCategory
// @Produce json
// @Param id path string true "StructCategory id"
// @Success 200 {object} models.StructCategory
// @Router /data-category/{id} [get]
func GetCategoryById(c *gin.Context) { // Get model if exist
	var category models.StructCategory

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": category})
}



// UpdateStructCategory godoc
// @Summary Update StructCategory.
// @Description Update StructCategory by id.
// @Tags StructCategory
// @Produce json
// @Param id path string true "StructCategory id"
// @Param Body body StructCategoryInput true "the body to update Category"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.StructCategory
// @Router /data-category/{id} [patch]
func UpdateCategory(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var category models.StructCategory
	if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input StructCategoryInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.StructCategory
	updatedInput.ID = input.ID
	updatedInput.NameCategory = input.NameCategory
	updatedInput.UpdatedAt = time.Now()

	db.Model(&category).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": category})
}

// DeleteStructCategory godoc
// @Summary Delete one StructCategory.
// @Description Delete a StructCategory by id.
// @Tags StructCategory
// @Produce json
// @Param id path string true "StructCategory id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /data-category/{id} [delete]
func DeleteCategory(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var category models.StructCategory
	if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&category)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
