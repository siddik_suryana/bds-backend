package main

import (
	"backend/config"
	"backend/docs"
	"backend/routes"
	"log"

	"github.com/joho/godotenv"
)

func main(){
	// programmatically set swagger info
	docs.SwaggerInfo.Title = "User Documentation API"
	docs.SwaggerInfo.Description = "This is User Documentation."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	// for load godotenv
    err := godotenv.Load()
    if err != nil {
        log.Fatal("Error loading .env file")
    }

	db := config.ConnectDataBase()
	sqlDB, _ := db.DB()
	defer sqlDB.Close()

	r := routes.SetupRouter(db)
	r.Run()
}