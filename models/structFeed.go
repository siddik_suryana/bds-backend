package models

import (
	"time"
)

type (
	StructFeed struct {
		ID              int       `json:"id" gorm:"primary_key"`
		Comment         string    `json:"comment"`
		StructUserID    int       `json:"userID"`
		StructProductID int       `json:"productID"`
		CreatedAt       time.Time `json:"created_at"`
		UpdatedAt       time.Time `json:"updated_at"`
		//migration
		StructUser    StructUser    `json:"-"`
		StructProduct StructProduct `json:"-"`

		
	}
)
