package controllers

import (
	"net/http"
	"time"

	"backend/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type StructUserInput struct {
	// ID          int     `json:"id" gorm:"primary_key"`
	// NameProduct string  `json:"name_product"`
	// Image       string  `json:"image"`
	// Price       string  `json:"price"`
	// Rating      float64 `json:"rating"`
	// Description string  `json:"description"`
	ID       int    `json:"id" gorm:"primary_key"`
	NameUser string `json:"name_user"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// GetAllStructUser godoc
// @Summary Get all StructUser.
// @Description Get a list of StructUser.
// @Tags StructUser
// @Produce json
// @Success 200 {object} []models.StructUser
// @Router /data-user [get]
func GetAllUser(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var users []models.StructUser
	db.Find(&users)

	c.JSON(http.StatusOK, gin.H{"data": users})
}

// CreateStructUser godoc
// @Summary Create New StructUser.
// @Description Creating a new StructUser.
// @Tags StructUser
// @Param Body body StructUserInput true "the body to create a new StructUser"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.StructUser
// @Router /data-user [post]
func CreateUser(c *gin.Context) {
	// Validate input
	var input StructUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create User
	user := models.StructUser{
		// ID:          input.ID,
		// NameProduct: input.NameProduct,
		// Image:       input.Image,
		// Price:       input.Price,
		// Rating:      input.Rating,
		// Description: input.Description,
		ID:input.ID,        
		NameUser:input.NameUser,  
		Email:input.Email,     
		Password:input.Password,  
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&user)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// GetStructUserById godoc
// @Summary Get StructUser.
// @Description Get an StructUser by id.
// @Tags StructUser
// @Produce json
// @Param id path string true "StructUser id"
// @Success 200 {object} models.StructUser
// @Router /data-user/{id} [get]
func GetUserById(c *gin.Context) { // Get model if exist
	var user models.StructUser

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// GetTransactionByUserID godoc
// @Summary Get Transaction.
// @Description Get all Transaction by StructUserID.
// @Tags StructUser
// @Produce json
// @Param id path string true "StructUser id"
// @Success 200 {object} []models.StructUser
// @Router /data-user/{id}/data-transaction [get]
func GetTransactionByUserId(c *gin.Context) { // Get model if exist
    var transaction []models.StructTransaction

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("user_ID = ?", c.Param("id")).Find(&transaction).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
		
    }

    c.JSON(http.StatusOK, gin.H{"data": transaction})
}

// GetProductByStructUserID godoc
// @Summary Get Product.
// @Description Get all Product by StructUserID.
// @Tags StructUser
// @Produce json
// @Param id path string true "StructProduct id"
// @Success 200 {object} []models.StructProduct
// @Router /data-product/{id}/data-user [get]
func GetProductByUserId(c *gin.Context) { // Get model if exist
	var product []models.StructProduct

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("userID = ?", c.Param("id")).Find(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return

	}

	c.JSON(http.StatusOK, gin.H{"data": product})
}


// UpdateStructUser godoc
// @Summary Update StructUser.
// @Description Update StructUser by id.
// @Tags StructUser
// @Produce json
// @Param id path string true "StructUser id"
// @Param Body body StructUserInput true "the body to update user"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.StructUser
// @Router /data-user/{id} [patch]
func UpdateUser(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var user models.StructUser
	if err := db.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input StructUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.StructUser
	updatedInput.ID = input.ID
	updatedInput.NameUser = input.NameUser
	updatedInput.Email = input.Email
	updatedInput.Password = input.Password
	updatedInput.UpdatedAt = time.Now()

	db.Model(&user).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// DeleteStructUser godoc
// @Summary Delete one StructUser.
// @Description Delete a StructUser by id.
// @Tags StructUser
// @Produce json
// @Param id path string true "StructUser id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /data-user/{id} [delete]
func DeleteUser(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var user models.StructUser
	if err := db.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&user)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
