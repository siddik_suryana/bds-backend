package config

import (
  "backend/models"
  "fmt"

  "gorm.io/driver/mysql"
  "gorm.io/gorm"
)

func ConnectDataBase() *gorm.DB {
  username := "root"
  password := ""
  host := "tcp(127.0.0.1:3306)"
  database := "db_shop"

  dsn := fmt.Sprintf("%v:%v@%v/%v?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, database)

  db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

  if err != nil {
    panic(err.Error())
  }

  db.AutoMigrate(&models.StructUser{}, &models.StructTransaction{}, &models.User{})
  db.AutoMigrate(&models.StructUser{}, &models.StructProduct{}, &models.User{})
  db.AutoMigrate(&models.StructUser{}, &models.StructFeed{}, &models.User{})
  db.AutoMigrate(&models.StructCategory{}, &models.StructProduct {}, &models.User{})
  db.AutoMigrate(&models.StructProduct{}, &models.StructFeed{}, &models.User{})

  return db
}
