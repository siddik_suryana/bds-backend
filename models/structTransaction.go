package models

import (
	"time"
)

type (
	StructTransaction struct {
		ID           int        `json:"id" gorm:"primary_key"`
		Payment      int        `json:"payment"`
		StructUserID int        `json:"userID"`
		CreatedAt    time.Time  `json:"created_at"`
		UpdatedAt    time.Time  `json:"updated_at"`
		StructUser   StructUser `json:"-"`
	}
)
