package models

import (
	"time"
)

type (
	StructCategory struct {
		ID               int            `json:"id" gorm:"primary_key"`
		NameCategory      string         `json:"name_category"`
		CreatedAt     time.Time       `json:"created_at"`
		UpdatedAt     time.Time       `json:"updated_at"`
		StructProduct []StructProduct `json:"-"`
		
	}
)
