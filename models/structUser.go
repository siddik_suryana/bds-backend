package models

import (
	"time"
)

type (
	StructUser struct {
		ID          int                 `json:"id" gorm:"primary_key"`
		NameUser    string              `json:"name_user"`
		Email       string              `json:"email"`
		Password    string              `json:"password"`
		CreatedAt   time.Time           `json:"created_at"`
		UpdatedAt   time.Time           `json:"updated_at"`
		Feed        []StructFeed        `json:"-"`
		Product     []StructProduct     `json:"-"`
		Transaction []StructTransaction `json:"-"`
	}
)
